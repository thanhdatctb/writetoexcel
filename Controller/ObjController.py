import pandas as pd
import xlsxwriter
import xlwt

from Model.Sheet1 import Sheet1


class ObjController:
    def __init__(self):
        self.DataSheet1 = self.getDataSheet1()

    def getDataSheet1(self):
        excel_data_df = pd.read_excel('File_input.xlsx', sheet_name='Sheet1')
        # print whole sheet data

        df = pd.DataFrame(excel_data_df)
        DataSheet1s = []
        for index, row in df.iterrows():
            sheet1 = Sheet1()
            sheet1.Ma_PO = row["Mã PO"]
            sheet1.Ma_SKU = row["Mã SKU"]
            sheet1.Don_gia = row["Đơn giá"]
            sheet1.SL_truoc_sua = row["SL trước sửa"]
            sheet1.SL_sau_sua = row["SL sau sửa"]
            sheet1.thanh_tien = row["Thành tiền"]
            sheet1.note = row["Note"]
            sheet1.so_HD = row["Số HD"]
            DataSheet1s.append(sheet1)
        return DataSheet1s

    def getTotalThanhTien(self,po):
        thanh_tien = 0
        for d in self.DataSheet1:
            if d.Ma_PO == po:
                thanh_tien += d.thanh_tien
        return thanh_tien


    def getTotalSKU(self, po):
        sku = 0
        for d in self.DataSheet1:
            if d.Ma_PO == po:
                sku += d.thanh_tien/d.SL_truoc_sua*d.SL_sau_sua
        return sku

    def getLastMoney(self, data):
        sku = self.getTotalSKU(data.Ma_PO)
        data
        last_money = int(data.Tong_tien_PO) - int(self.getTotalThanhTien()) + sku
        return last_money

    def writeToExcel(self, list):
        with xlsxwriter.Workbook('File_output.xlsx') as workbook:
            worksheet = workbook.add_worksheet("Sheet1")
            #worksheet.write(1, 0, list[0].Status)
            # return

            cell_format = workbook.add_format({'bold': True, 'font_color': 'black'})
            # Writing on specified sheet
            worksheet.write(0, 0, "Status", cell_format)
            count = 0
            worksheet.write(0, count, "Status", cell_format)
            count += 1
            worksheet.write(0, count, "Tên doanh nghiệp", cell_format)
            count += 1
            worksheet.write(0, count, "Số mẫu hóa đơn", cell_format)
            count += 1
            worksheet.write(0, count, "Ký hiệu hóa đơn", cell_format)
            count += 1
            worksheet.write(0, count, "Số hóa đơn", cell_format)
            count += 1
            worksheet.write(0, count, "Ngày", cell_format)
            count += 1
            worksheet.write(0, count, "Mã số thuế người bán/người mua", cell_format)
            count += 1
            worksheet.write(0, count, "Công Ty", cell_format)
            count += 1
            worksheet.write(0, count, "Địa chỉ", cell_format)
            count += 1
            worksheet.write(0, count, "Mã hàng hóa/Dịch vụ", cell_format)
            count += 1
            worksheet.write(0, count, "Tên hàng hóa/Dịch vụ/Diễn giải", cell_format)
            count += 1
            worksheet.write(0, count, "Đơn vị tính", cell_format)
            count += 1
            worksheet.write(0, count, "Số lượng", cell_format)
            count += 1
            worksheet.write(0, count, "Đơn giá", cell_format)
            count += 1
            worksheet.write(0, count, "Thuế VAT Mặt Hàng (%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tiền thuế VAT Mặt hàng", cell_format)
            count += 1
            worksheet.write(0, count, "Thành tiền", cell_format)
            count += 1
            worksheet.write(0, count, "Cộng tiền HÀng Hóa(0%)", cell_format)
            count += 1
            worksheet.write(0, count, "Thuế VAT hóa đơn (0%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tiền thuế VAT hoá đơn (0%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tổng tiền thanh toán (0%)", cell_format)
            count += 1
            worksheet.write(0, count, "Cộng tiền HÀng Hóa(5%)", cell_format)
            count += 1
            worksheet.write(0, count, "Thuế VAT hóa đơn (5%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tiền thuế VAT hoá đơn (5%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tổng tiền thanh toán (5%)", cell_format)
            count += 1
            worksheet.write(0, count, "Cộng tiền HÀng Hóa(10%)", cell_format)
            count += 1
            worksheet.write(0, count, "Thuế VAT hóa đơn (10%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tiền thuế VAT hoá đơn (10%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tổng tiền thanh toán (10%)", cell_format)
            count += 1
            worksheet.write(0, count, "Cộng tiền HÀng Hóa(...%)", cell_format)
            count += 1
            worksheet.write(0, count, "Thuế VAT hóa đơn (...%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tiền thuế VAT hoá đơn (...%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tổng tiền thanh toán (...%)", cell_format)
            count += 1
            worksheet.write(0, count, "Tổng cộng tiền Hàng Hóa", cell_format)
            count += 1
            worksheet.write(0, count, "Tổng cộng tiền thuế VAT hoá đơn", cell_format)
            count += 1
            worksheet.write(0, count, "Tổng cộng tiền thanh toán", cell_format)
            count += 1
            worksheet.write(0, count, "Hình thức thanh toán", cell_format)
            count += 1
            worksheet.write(0, count, "Mã PO", cell_format)
            count += 1
            worksheet.write(0, count, "Tổng tiền PO", cell_format)
            count += 1
            worksheet.write(0, count, "Thực Trạng", cell_format)
            count += 1
            worksheet.write(0, count, "Mã Vendor", cell_format)
            count += 1
            worksheet.write(0, count, "HDgroup", cell_format)
            count += 1
            worksheet.write(0, count, "Tên ảnh", cell_format)
            count += 1
            worksheet.write(0, count, "Receiver", cell_format)
            count += 1

            for row_num, data in enumerate(list):
                #worksheet.write_row(list.index(data)+1, 0, data)
                row = list.index(data)+1
                # status =
                #worksheet.write(row, 0, str(data.Status))
                count_item = 0
                worksheet.write(row, count_item, str(data.Status))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ten_doanh_nghiep))
                count_item += 1
                worksheet.write(row, count_item, str(data.So_mau_hoa_don))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ky_hieu_hoa_đon))
                count_item += 1
                worksheet.write(row, count_item, str(data.So_hoa_don))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ngay))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ma_so_thue_nguoi_ban_nguoi_mua))
                count_item += 1
                worksheet.write(row, count_item, str(data.Cong_ty))
                count_item += 1
                worksheet.write(row, count_item, str(data.Dia_chi))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ma_hang_hoa_dich_vụ))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ten_hang_hoa_dịch_vu_dien_giai))
                count_item += 1
                worksheet.write(row, count_item, str(data.Don_vi_tinh))
                count_item += 1
                worksheet.write(row, count_item, str(data.So_luong))
                count_item += 1
                worksheet.write(row, count_item, str(data.Don_gia))
                count_item += 1
                worksheet.write(row, count_item, str(data.Thue_VAT_mat_Hang))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tien_thue_VAT_Mat_hang))
                count_item += 1
                worksheet.write(row, count_item, str(data.Thanh_tien))
                count_item += 1
                worksheet.write(row, count_item, str(data.Cong_tien_Hang_Hoa))
                count_item += 1
                worksheet.write(row, count_item, str(data.Thue_VAT_hoa_don))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tien_thue_VAT_hoa_don_0))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tong_tien_thanh_toan_0))
                count_item += 1
                worksheet.write(row, count_item, str(data.Cong_tien_Hang_Hoa_5))
                count_item += 1
                worksheet.write(row, count_item, str(data.Thue_VAT_hoa_don_5))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tien_thue_VAT_hoa_don_5))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tong_tien_thanh_toan_5))
                count_item += 1
                worksheet.write(row, count_item, str(data.Cong_tien_hang_hoa_10))
                count_item += 1
                worksheet.write(row, count_item, str(data.Thue_VAT_hoa_don_10))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tien_thue_VAT_hoa_don_10))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tong_tien_thanh_toan_10))
                count_item += 1
                worksheet.write(row, count_item, str(data.Cong_tien_hang_hoa))
                count_item += 1
                worksheet.write(row, count_item, str(data.Thue_VAT_hoa_don))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tien_thue_VAT_hoa_don))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tong_tien_thanh_toan))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tong_cong_tien_hang_hoa))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tong_cong_tien_thue_VAT_hoa_don))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tong_cong_tien_thanh_toan))
                count_item += 1
                worksheet.write(row, count_item, str(data.Hinh_thuc_thanh_toan))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ma_PO))
                count_item += 1
                worksheet.write(row, count_item, str(data.Tong_tien_PO))
                count_item += 1
                worksheet.write(row, count_item, str(data.Thuc_Trang))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ma_Vendor))
                count_item += 1
                worksheet.write(row, count_item, str(data.HDgroup))
                count_item += 1
                worksheet.write(row, count_item, str(data.Ten_anh))
                count_item += 1
                worksheet.write(row, count_item, str(data.Receiver))
                count_item += 1
                print("Đã in ra file")

