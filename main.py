import pandas as pd

from Controller.ObjController import ObjController
from Model.MyOjb import MyOjb

excel_data_df = pd.read_excel('File_input.xlsx', sheet_name='Sheet')
# print whole sheet data

df = pd.DataFrame(excel_data_df)
myList = []
for index, row in df.iterrows():

    #print(row["Tên doanh nghiệp"])
    myOjb = MyOjb()
    myOjb.Status = row["Status"]
    myOjb.Ten_doanh_nghiep = row["Tên doanh nghiệp"]
    myOjb.So_mau_hoa_don = row["Số mẫu hóa đơn"]
    myOjb.Ky_hieu_hoa_đon = row["Ký hiệu hóa đơn"]
    myOjb.So_hoa_don = row["Số hóa đơn"]
    myOjb.Ngay = row["Ngày"]
    myOjb.Ma_so_thue_nguoi_ban_nguoi_mua = row["Mã số thuế người bán/người mua"]
    myOjb.Cong_ty = row["Công Ty"]
    myOjb.Dia_chi = row["Địa chỉ"]
    myOjb.Ma_hang_hoa_dich_vụ = row["Mã hàng hóa/Dịch vụ"]
    myOjb.Ten_hang_hoa_dịch_vu_dien_giai = row["Tên hàng hóa/Dịch vụ/Diễn giải"]
    myOjb.Don_vi_tinh = row["Đơn vị tính"]
    myOjb.So_luong = row["Số lượng"]
    myOjb.Don_gia = row["Đơn giá"]
    myOjb.Thue_VAT_mat_Hang = row["Thuế VAT Mặt Hàng (%)"]
    myOjb.Tien_thue_VAT_Mat_hang = row["Tiền thuế VAT Mặt hàng"]
    myOjb.Thanh_tien = row["Thành tiền"]
    myOjb.Cong_tien_Hang_Hoa = row["Cộng tiền HÀng Hóa(0%)"]
    myOjb.Thue_VAT_hoa_don = row["Thuế VAT hóa đơn (0%)"]
    myOjb.Tien_thue_VAT_hoa_don_0 = row["Tiền thuế VAT hoá đơn (0%)"]
    myOjb.Tong_tien_thanh_toan_0 = row["Tổng tiền thanh toán (0%)"]
    myOjb.Cong_tien_Hang_Hoa_5 = row["Cộng tiền HÀng Hóa(5%)"]
    myOjb.Thue_VAT_hoa_don_5 = row["Thuế VAT hóa đơn (5%)"]
    myOjb.Tien_thue_VAT_hoa_don_5 = row["Tiền thuế VAT hoá đơn (5%)"]
    myOjb.Tong_tien_thanh_toan_5  = row["Tổng tiền thanh toán (5%)"]
    myOjb.Cong_tien_hang_hoa_10 = row["Cộng tiền HÀng Hóa(10%)"]
    myOjb.Thue_VAT_hoa_don_10  = row["Thuế VAT hóa đơn (10%)"]
    myOjb.Tien_thue_VAT_hoa_don_10 = row["Tiền thuế VAT hoá đơn (10%)"]
    myOjb.Tong_tien_thanh_toan_10 = row["Tổng tiền thanh toán (10%)"]
    myOjb.Cong_tien_hang_hoa = row["Cộng tiền HÀng Hóa(...%)"]
    myOjb.Thue_VAT_hoa_don = row["Thuế VAT hóa đơn (...%)"]
    myOjb.Tien_thue_VAT_hoa_don = row["Tiền thuế VAT hoá đơn (...%)"]
    myOjb.Tong_tien_thanh_toan = row["Tổng tiền thanh toán (...%)"]
    myOjb.Tong_cong_tien_hang_hoa = row["Tổng cộng tiền Hàng Hóa"]
    myOjb.Tong_cong_tien_thue_VAT_hoa_don = row["Tổng cộng tiền thuế VAT hoá đơn"]
    myOjb.Tong_cong_tien_thanh_toan = row["Tổng cộng tiền thanh toán"]
    myOjb.Hinh_thuc_thanh_toan = row["Hình thức thanh toán"]
    myOjb.Ma_PO = row["Mã PO"]
    myOjb.Tong_tien_PO = row["Tổng tiền PO"]
    myOjb.Thuc_Trang = row["Thực Trạng"]
    myOjb.Ma_Vendor = row["Mã Vendor"]
    myOjb.HDgroup = row["HDgroup"]
    myOjb.Ten_anh = row["Tên ảnh"]
    myOjb.Receiver = row["Receiver"]

    myList.append(myOjb)
    #print(row['Ký hiệu hóa đơn'])
for e in myList:
    e.standart()

controller = ObjController()
controller.writeToExcel(myList)
for e in controller.DataSheet1:
    print(e.Ma_PO)



